# Contact Storage Disable Mail

Add the ability to disable email notifications on a per-contact-form basis.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/contact_storage_dm).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/contact_storage_dm).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- [Contact Storage](https://www.drupal.org/project/contact_storage).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

To access the option, go to edit any contact form, and it should appear there.

## Maintainers

- Ivan Duarte - [jidrone](https://www.drupal.org/u/jidrone)
